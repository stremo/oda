FROM node:17-alpine

RUN mkdir -p /opt/oda
WORKDIR /opt/oda
COPY ./package.json ./package.json
COPY ./dist ./dist
RUN npm install --only=prod

EXPOSE 8080

ENTRYPOINT ["npm", "start"]
