import {getHtmlPage} from "../htmlPageClient/htmlPageClientProvider";
import {parse} from "node-html-parser";

const getSchemaOrgDataFromHtml = (html: string): any[] => {

    const root = parse(html);
    return Array.from(root.querySelectorAll("script[type='application/ld+json']")).map(it => JSON.parse(it.innerText)).filter(it => it.hasOwnProperty("@context") && (it["@context"] as string).includes("schema.org"));
}

export const getSchemaOrgDataFromSite = (url: string): Promise<any[]> => {
    return getHtmlPage(url)
        .then(html => Promise.resolve(getSchemaOrgDataFromHtml(html)))
        .catch(() => Promise.reject("could not get data from html site"))
};