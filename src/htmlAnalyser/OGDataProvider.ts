import {getHtmlPage} from "../htmlPageClient/htmlPageClientProvider";
import {parse} from "node-html-parser";

export interface OpenGraphEntry {
    readonly name: string;
    readonly value: string;
}

export const getOpenGraphDataFromHtml = (html: string): OpenGraphEntry[] => {
    const root = parse(html);
    return Array.from(root.querySelectorAll("meta[property^='og:']")).map(it => metaTagToOpenGraphEntry(it));
}

export const getOpenGraphDataFromSite = (url: string): Promise<OpenGraphEntry[]> =>
    getHtmlPage(url)
        .then(html => Promise.resolve(getOpenGraphDataFromHtml(html)))
        .catch(() => Promise.reject("could not get data from html site"))

const metaTagToOpenGraphEntry = (tag: any): OpenGraphEntry => ({
    name: tag.getAttribute("property").substring(3),
    value: tag.getAttribute("content")
});
