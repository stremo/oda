import express from "express";
import {getOpenGraphDataFromSite} from "./htmlAnalyser/OGDataProvider";
import {getSchemaOrgDataFromSite} from "./htmlAnalyser/SchemaOrgDataProvider";
import {extractFirstProductInformation} from "./productInformation/productInformationExtractor";

const port = 8080;

const server: express.Application = express();

server.get("/", (req, res) => {
    res.send([
        {
            path: "/api/ogData",
            method: "GET",
            description: "provides all open graph data from a given url",
            queryParameters: [
                {
                    name: "site",
                    description: "url of target site",
                    required: true,
                }
            ]
        },
        {
            path: "/api/schemaOrgData",
            description: "provides all schema.org data from a given url",
            method: "GET",
            queryParameters: [
                {
                    name: "site",
                    description: "url of target site",
                    required: true,
                },
            ]
        },
        {
            path: "/api/productInformation",
            description: "provides all schema.org based data from a given url and extract only the necessary information about the product",
            method: "GET",
            queryParameters: [
                {
                    name: "site",
                    description: "url of target site",
                    required: true,
                },
            ]
        }
    ])
})

server.get("/api/ogData", (req, res) => {
    const params = req.query;
    const url = params["site"] as string;

    if (url === undefined || url.trim().length === 0) {
        res.status(400);
        res.send("query parameter site is required!");
        return;
    }

    getOpenGraphDataFromSite(url)
        .then(data => res.send(data))
        .catch(e => {
            res.status(400);
            res.send(e);
        });
});

server.get("/api/schemaOrgData", (req, res) => {
    const params = req.query;
    const url = params["site"] as string;
    // const type = params["type"] as string;

    if (url === undefined || url.trim().length === 0) {
        res.status(400);
        res.send("query parameter site is required!");
        return;
    }

    getSchemaOrgDataFromSite(url)
        .then(data => res.send(data))
        .catch(e => {
            res.status(400);
            res.send(e);
        });
});
server.get("/api/productInformation", (req, res) => {
    const params = req.query;
    const url = params["site"] as string;
    // const type = params["type"] as string;

    if (url === undefined || url.trim().length === 0) {
        res.status(400);
        res.send("query parameter site is required!");
        return;
    }

    getSchemaOrgDataFromSite(url)
        .then(data => res.send(extractFirstProductInformation(data)))
        .catch(e => {
            res.status(400);
            res.send(e);
        });
});


server.listen(port, () => {
    console.log("oda server started on port: " + port);
});

