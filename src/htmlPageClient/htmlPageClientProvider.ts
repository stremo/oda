import axios from "axios";

export const getHtmlPage = (url: string): Promise<string> => {
    return axios.get(url, {responseType: "text"})
        .then(r => Promise.resolve(r.data))
        .catch(e => Promise.reject(e));
};
