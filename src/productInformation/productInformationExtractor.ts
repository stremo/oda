const productTypeValue = "Product";

export interface ProductInformation {
    readonly name: string;
    readonly description?: string;
    readonly imageUrls: string[];
    readonly brand: string;
}


export const extractFirstProductInformation = (schemaEntries: any[]): ProductInformation | undefined => {
    // check if we have to enter the graph section first
    if (schemaEntries.find(it => Object.keys(it).includes("@graph"))) {
        const entries = schemaEntries.find(it => Object.keys(it).includes("@graph"))["@graph"];
        return extractFirstProductInformation(entries)
    }

    const productEntry = schemaEntries.find(it => it["@type"] === productTypeValue);

    if (productEntry === undefined) {
        return undefined
    }

    const imageValue = productEntry["image"]
    return {
        name: productEntry["name"],
        description: productEntry["description"],
        imageUrls: Array.isArray(imageValue) ? imageValue : [imageValue],
        brand: productEntry["brand"]["name"]
    };
}

