# Oda

open graph data analyser: a service providing data from open graph fields from web sites

## use Oda as an user

Oda provides an single rest endpoint:

* METHOD: `GET`
* PATH: `/api/ogData`
* parameters: 
  * `site`: url encoded target url from site you want to scan


## using Oda as a developer

* run `npm install` to install all required parameters
* run `npm run bAS` to build and start the application
* run `npm run buildDockerImage` to build the docker image with name `registry.gitlab.com/stremo/oda:latest`


## running docker container

`docker run -d -p 9999:8080 registry.gitlab.com/stremo/oda:latest `